package com.kakaruu.example.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityRetrainApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityRetrainApplication.class, args);
	}

}
