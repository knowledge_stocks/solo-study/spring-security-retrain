package com.kakaruu.example.security.service;

import com.kakaruu.example.security.entity.User;
import com.kakaruu.example.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username) {
        Optional<User> optionalUser = userRepository.findOneWithAuthoritiesByUsername(username);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException(username + " -> 데이터베이스에서 찾을 수 없습니다."));

        if(!user.getActivated()) {
            throw new RuntimeException(username + " -> 활성화되어 있지 않습니다.");
        }

        return user;
    }
}
