package com.kakaruu.example.security.repository;

import com.kakaruu.example.security.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    // EntityGraph 어노테이션을 설정하면 Lazy 조회 대신 Eager 조회를 하게 된다.
    @EntityGraph(attributePaths = "authorities")
    public Optional<User> findOneWithAuthoritiesByUsername(String username);
}
